/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nitroez.homework1;

import java.util.Scanner;

/**
 *
 * @author ckitt
 */
public class GameOX {
    public static void printTable(String tb[][]) {
        int row = 1, col = 1;
        for (int k = 0; k < 4; k++) {
            if (k != 0) {
                System.out.print(col + " ");
                col++;
            } else
                System.out.print("  ");
        }
        System.out.println("");
        for (int i = 0; i < 3; i++) {
            System.out.print(row + " ");
            for (int j = 0; j < 3; j++) {
                System.out.print(tb[i][j] + " ");
            }
            row++;
            System.out.println("");
        }
    }

    public static boolean CheckInput(int row, int col, String tb[][]) {
        if (!tb[row][col].equals("-"))
            return false;
        return true;
    }

    public static boolean CheckWin(String tb[][]){
        if((tb[0][0].equals(tb[0][1]))&& (tb[0][0].equals(tb[0][2])) && !tb[0][0].equals("-")){
            return true;
        }else if((tb[1][0].equals(tb[1][1]))&&(tb[1][0].equals(tb[1][2]))&& !tb[1][0].equals("-")){
            return true;
        }else if((tb[2][0].equals(tb[2][1]))&&(tb[2][0].equals(tb[2][2]))&& !tb[2][0].equals("-")){
            return true;
        }else if((tb[0][0].equals(tb[1][0]))&&(tb[0][0].equals(tb[2][0]))&& !tb[0][0].equals("-")){
            return true;
        }else if((tb[0][1].equals(tb[1][1]))&&(tb[0][1].equals(tb[2][1]))&& !tb[0][1].equals("-")){
            return true;
        }else if((tb[0][2].equals(tb[1][2]))&&(tb[0][2].equals(tb[2][2]))&& !tb[0][2].equals("-")){
            return true;
        }else if((tb[0][0].equals(tb[1][1]))&&(tb[0][0].equals(tb[2][2]))&& !tb[0][0].equals("-")){
            return true;
        }else if((tb[0][2].equals(tb[1][1]))&&(tb[0][2].equals(tb[2][0]))&& !tb[0][2].equals("-")){
            return true;
        }
        return false;
    }

    public static boolean checkNum(String x){
        if(x.equals("1")||x.equals("2")||x.equals("3")){
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        System.out.println("Welcome to OX Game");

        String[][] table = new String[3][3];
        // **Set default**
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                table[i][j] = "-";
            }
        }
        boolean winner = false;
        printTable(table);
        int count = 1;
        while (!winner && count<10) {
            if (count % 2 == 1) {
                System.out.println("X Turn");
            } else {
                System.out.println("O Turn");
            }
            System.out.println("Please input Row Col :");
            int row,col;
            String rowg,colg;
            
            //**check input row is 1 2 3 */
            rowg=kb.next();
            while(!checkNum(rowg)){
                System.out.println("Please Input 1, 2 or 3");
                System.out.println("Input Row:");
                rowg=kb.next();
            }
            row=Integer.parseInt(rowg);

            //**check input col is 1 2 3 */
            colg=kb.next();
            while(!checkNum(colg)){
                System.out.println("Please Input 1, 2 or 3");
                System.out.println("Input Col:");
                colg=kb.next();
            }
            col=Integer.parseInt(colg);

            //**Check Input this position is empty */
            while (!CheckInput(row-1, col-1, table)) {
                System.out.println("Re-Input");
                System.out.println("Please input Row Col :");
                row = kb.nextInt();
                col = kb.nextInt();
            }
            
            if (count % 2 == 1) {
                table[row - 1][col - 1] = "X";
            } else {
                table[row - 1][col - 1] = "O";
            }

            printTable(table);
            
            if(count>4){
                if(CheckWin(table)){
                    winner=true;
                    if(count%2==1){
                        System.out.println("X Win");
                    }else{
                        System.out.println("O Win");
                    }
                    System.out.println("Bye bye");
                }
            }
            
            count++;
        }

        if(count==10&& !winner){
            System.out.println("Draw");
        }
    }
}
